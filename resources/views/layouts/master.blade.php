<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Khmerin</title>

    <!-- ======================= Love Think Learn Do It =========================== -->

    <!-- ================ Start Link External file Area ================ -->

    <!-- Link bootstrap file css -->
    <link rel="stylesheet" href="{{('lib/bootstrap/css/bootstrap.min.css')}}">

    <!-- Link iconmoon file css -->
    <link rel="stylesheet" href="{{('lib/css/icomoon.css')}}">

    <!-- Link custom style file css -->
    <link rel="stylesheet" href="{{('assets/css/custom-style.css')}}">

    <!-- Link compenent krb file css -->
    <link rel="stylesheet" href="{{('assets/css/compenent-krb-effect.css')}}">

    <!-- Link responsive file css -->
    <link rel="stylesheet" href="{{('assets/css/responsive.css')}}">

    <!-- ================ End   Link External file Area ================ -->


</head>

<body>
    <!--  -->
    <div id="krb-dev">
        <!-- ================ Start Header Area ================ -->
        <header class="header_area">
            <nav class="container-fluid bg-danger navbar-dark fixed-top">
                <div class="row">
                    <div class="col-xl-2 col-lg-2 col-md-12 col-sm-12">
                        <!-- ======= Brand Area ======= -->
                        <center>
                            <div class="brand">
                                <a href="" class="navbar-brand">
                                    <img src="https://cdn.clipart.email/04610ffdea1cec480ad84f6e38c4b161_musical-instrument-clip-art-at-clkercom-vector-clip-art-online-_528-598.png" width="40" alt=""> Khmerin
                                    <!-- <span class="title-text">Khmerin</span> -->
                                </a>
                            </div>
                        </center>
                        <!-- ======= End Brand Area ======= -->
                    </div>
                    <div class="col-xl-6 col-lg-5 col-md-6 col-sm-3">
                        <!-- ======= Search compenent Area ======= -->
                        <div class="search-bar">
                            <input type="text" placeholder="Search">
                            <button><i class="icon icon-search4"></i></button>
                        </div>
                        <!-- ======= End Search compenent Area ======= -->
                    </div>
                    <!-- ======= Main Menu Area ======= -->
                    <div class="col-xl-2 col-lg-3 menu-responsive">
                        <div class="navbar main-menu float-right">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Document</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Post</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="#">Event</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- ======= End Main Menu Area ======= -->

                    <div class="col-xl-2 col-lg-2 col-md-6 col-sm-3">
                        <!-- ======= Accountation Area ======== -->
                        <div class="navbar">
                            <ul class="nav">
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <img src="https://z-p3-scontent.fpnh16-1.fna.fbcdn.net/v/t1.0-9/83875004_1042376972802835_5804157746559320064_n.jpg?_nc_cat=100&_nc_eui2=AeEKJAgaw9o5s_muZRGFIneruTS6On83CAltRgoed4CtwYR0e0faq5KoBnU8qSZEbVtN6BRdo6rflaUNdnJqdAm2Ma9Wo716T4-kPJc611AzQA&_nc_ohc=LrpN9SzIyaAAX9bZLML&_nc_ht=z-p3-scontent.fpnh16-1.fna&oh=a88908c7410c82becc3830e13396bc3e&oe=5E9027B3" alt="" width="30" class="rounded-circle">
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="" class="nav-link">
                                        <span style="font-size: 15px; color: whitesmoke">Sokbuntheoun</span>
                                    </a>
                                </li>
                                <li class="nav-item dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false" style="color: whitesmoke; font-size: 25px"></a>
                                    <div class="dropdown-menu dropdown-menu-right">
                                        <a class="dropdown-item" href="#">Pofile</a>
                                        <a class="dropdown-item" href="#">Notification</a>
                                        <a class="dropdown-item" href="#">Something else here</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item" href="#">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <!-- ======= End Accountation Area ======== -->
                    </div>
                </div>
            </nav>
        </header>
        <!-- ================ End   Header Area ================ -->

        <div class="divide-section" style="height: 80px"></div>

        <!-- ================ Start Main Area ================ -->
        <section class="main_area">
            <div class="container">
                <div class="wraper d-flex flex-row" style="width: 100%; position: relative">
                    <aside class="content_area bg-success" style="width: 75%; height: 100vh;">
                        2
                    </aside>
                    <div class="divide-aside" style="width: 2.5%">
                        1
                    </div>
                    <aside class="menu_area bg-primary " style="width: 20%; position: absolute; right: 0">
                        3
                    </aside>
                </div>
            </div>
        </section>
        <!-- ================ End   Main Area ================ -->

        <!-- ================ Start Footer Area ================ -->
        <footer class="footer_area">

        </footer>
        <!-- ================ End   Footer Area ================ -->
    </div>

    <!-- ================ Start External File Area ================ -->

    <!-- JQUERY file -->
    <script src="{{('assets/js/jquery-2.2.4.min.js')}}"></script>

    <!-- Bootstrap js file -->
    <script src="{{('lib/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- ================ End   External File Area ================ -->

</body>

</html>