// import 'jquery-2.2.4.min.js';

$(document).ready(function () {
    var btn_menu = $('#btn-menu');
    var btn_menu_clicked = 0;

    var krb_side_menu_bar = $('.krb-side-menu-bar');
    var krb_page_content = $('.krb-page-content');
    var krb_side_top_bar = $('.krb-side-top-bar');
    var scroll_available = $('.scroll-available');
    var krb_footer_side_menu_bar = $('.krb-footer-side-menu-bar');
    var size_of_bar_menu = '';
    var width = $(window).width();

    if (width < 575.98) {
        size_of_bar_menu = '50%';
        size_of_content = '50%';
        krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
    } else if (width >= 576 && width < 767.98) {
        size_of_bar_menu = '50%';
        size_of_content = '50%';
        krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
    } else if (width >= 768 && width < 991.98) {
        size_of_bar_menu = '40%';
        size_of_content = '60%';
        krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
    }
    else if (width >= 992 && width < 1199.98) {
        krb_side_menu_bar.css({ 'width': '20% !important', 'display': 'inline-block', 'transaction': '0.5s' });
        krb_page_content.css({ 'left': '20%', 'transaction': '0.5s' });
        krb_side_top_bar.css({ 'left': '20%', 'transaction': '0.5s' });
    } else if (width >= 1200) {
        krb_page_content.css({ 'left': '20%', 'transaction': '0.5s' });
        krb_side_menu_bar.css({ 'width': '20% !important', 'display': 'inline-block', 'transaction': '0.5s' });
    }

    $(window).resize(function () {
        width = $(window).width();
        if (width < 575.98) {
            size_of_bar_menu = '50%';
            size_of_content = '50%';
            krb_page_content.css({ 'width': '100%', 'transaction': '0.5s' });
            krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
            location.reload();
        } else if (width >= 576 && width < 767.98) {
            size_of_bar_menu = '50%';
            size_of_content = '50%';
            krb_page_content.css({ 'width': '100%', 'transaction': '0.5s' });
            krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
            location.reload();

        } else if (width >= 768 && width < 991.98) {
            size_of_bar_menu = '40%';
            size_of_content = '60%';
            krb_page_content.css({ 'width': '100%', 'transaction': '0.5s' });
            krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
            location.reload();

        }
        else if (width >= 992 && width < 1199.98) {
            size_of_bar_menu = '20%';
            size_of_content = '80%';
            krb_page_content.css({ 'left': '20%', 'transaction': '0.5s' });
            krb_side_menu_bar.css({ 'width': '20% !important', 'display': 'inline-block', 'transaction': '0.5s' });
            krb_side_top_bar.css({ 'left': '20%', 'transaction': '0.5s' });
            location.reload();
        } else if (width >= 1200) {
            size_of_bar_menu = '20%';
            size_of_content = '80%';
            krb_page_content.css({ 'left': '20%', 'transaction': '0.5s' });
            krb_side_menu_bar.css({ 'width': '20% !important', 'display': 'inline-block', 'transaction': '0.5s' });
            krb_side_top_bar.css({ 'left': '20%', 'transaction': '0.5s' });
            location.reload();
        }
    });

    btn_menu.click(function () {
        if (btn_menu_clicked == 0) {
            scroll_available.css({ 'width': size_of_bar_menu });
            krb_footer_side_menu_bar.css({ 'width': size_of_bar_menu });
            krb_side_menu_bar.css({ 'width': size_of_bar_menu, 'display': 'inline-block', 'transaction': '1s' });
            krb_page_content.css({ 'left': size_of_bar_menu, 'transaction': '1s' });
            krb_side_top_bar.css({ 'left': size_of_bar_menu, 'transaction': '1s' });
            btn_menu_clicked = 1;
        } else {
            krb_side_menu_bar.css({ 'display': 'none', 'transaction': '0.5s' });
            krb_page_content.css({ 'left': '0', 'transaction': '0.5s' });
            krb_side_top_bar.css({ 'left': '0', 'transaction': '0.5s' });
            btn_menu_clicked = 0;
        }
    })
});