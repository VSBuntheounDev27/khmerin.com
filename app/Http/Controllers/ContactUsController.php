<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index(){
        $name = 'Mr. Buntheoun';
        // return view('contact')->with('name',$name);

        return view('contact',['name'=>'Mr. Buntheoun', 'email'=>'buntheoun@gmail.com']);
    }
}
